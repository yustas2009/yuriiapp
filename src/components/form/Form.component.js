import React from 'react';
import { FormGroup, FormControl, HelpBlock } from 'react-bootstrap';
import { MainButton } from '../common/Buttons/MainButton';


class ArticleForm extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      title: '',
      text: '',
    };
  }

  handleTitleChange = (e) => {
    this.setState({ title: e.target.value });
  }

  handleTextChange = (e) => {
    this.setState({ text: e.target.value });
  }

  render() {
    return (
      <form onSubmit={(e) => this.props.onSubmit(e, this.state.title, this.state.text, this.props.articles)}>
        <FormGroup
          controlId="formBasicText"
        >
          <FormControl
            type="text"
            value={this.state.title}
            placeholder="Enter text"
            onChange={this.handleTitleChange}
          />
          <FormControl.Feedback />
          <HelpBlock>Give it a short name</HelpBlock>
          <FormControl 
            componentClass="textarea" 
            placeholder="textarea" 
            value={this.state.text}
            onChange={this.handleTextChange}
          />
          <FormControl.Feedback />
          <HelpBlock>Type the best article you can write</HelpBlock>
        </FormGroup>
        <MainButton type="submit" name="Submit" bsStyle="primary" color="#ec6088" />
      </form>
    );
  }
}

export default ArticleForm;



