import { Button } from 'react-bootstrap';
import styled from 'styled-components';

export const MainButtonStyled = styled(Button)`
  && {
      height: 30px;
      border-radius: 20px;
      border: none;
      outline: none;
      cursor: pointer;
      background-color: ${props => props.color};   
      &:focus {
        outline: none;
      }
  }
`;