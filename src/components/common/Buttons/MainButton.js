import React from 'react';
import * as styled from './Buttons.styled'

export const MainButton = (props) => {
  return(
    <styled.MainButtonStyled {...props}>
      { props.name }
    </styled.MainButtonStyled>
  );
}