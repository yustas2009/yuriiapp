import styled from 'styled-components';  

export const ArticlesListWrap = styled.div`
  && {
      padding: 50px 0;
  }
`;

export const ArticleUl = styled.ul`
  && {
      padding: 0;
      margin: 0;
      border-bottom: 1px solid #a0a0a0;
  }
`;

export const ArticleStyledLi = styled.li`
  && {
      display: flex;
      justify-content: flex-start;
      font-size: 20px;
      color: #ec6088;
      cursor: pointer;
      padding: 35px 20px;
      border-top: 1px solid #a0a0a0;
      list-style: none;
      &:hover {
        background-color: #f7f7f7;
      }
  }
`;

