import React from 'react';
import { ArticleStyledLi } from './ArticlesList.styled'

export const Article = (props) => {
  const { id, onClick, title } = props;
  return(
    <ArticleStyledLi id={id} onClick={onClick}>
      { title }
    </ArticleStyledLi>
  );
}