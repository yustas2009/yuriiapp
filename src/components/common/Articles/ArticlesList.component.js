import React from 'react';
import { ArticlesListWrap, ArticleUl } from './ArticlesList.styled';
import { Article } from './Article.component';
import { withRouter } from "react-router-dom";

class ArticlesList extends React.Component {
  clickHandler = (e, id) => {
    e.preventDefault();
    this.props.history.push(`/article/${id}`);
  }

  truncateLastThree = (data) => {
    return data.length + 1 > this.props.amount ? data.slice(data.length - this.props.amount).reverse() : data;
  }

  render() {
   
      if (!this.props.articles){
        return null;
      }
      return(
        <ArticlesListWrap>
          <ArticleUl>
            { 
              this.props.amount ?
                this.truncateLastThree(this.props.articles).map((article) => <Article key={article.id} title={article.title} onClick={(e) => this.clickHandler(e, article.id)} />)  :
                this.state.articles.isArray() && this.state.articles.map((article) => <Article key={article.id} title={article.title} onClick={(e) => this.clickHandler(e, article.id)} />) 
            }
          </ArticleUl>
        </ArticlesListWrap>  
      );
  }
}

export default withRouter(ArticlesList);

