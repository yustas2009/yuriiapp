import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { MainPage } from './features/main';
import { ArticlePage } from './features/article';
import { AddPage } from './features/addArticle';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <Route exact path="/" component={MainPage} />
            <Route path="/article/:id" component={ArticlePage} />
            <Route path="/add" component={AddPage} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
