import React from 'react';
import { AddPageWrap } from './AddPage.styled';
import { getFromLocalStorage, setLocalStorage } from '../../util/localStorage';
import Form from '../../components/form/Form.component';
import ArticlesList from '../../components/common/Articles/ArticlesList.component';



export class AddPageContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            count: 0,
            articles: [],
        }
    }

    componentDidMount() {
        let myData = getFromLocalStorage('myData');
        myData && this.setState({ articles: myData }); 
    }

    handleSubmit = (e, title, text, articles) => {
        e.preventDefault();
        if (title && text) {
            const newArticle = {
                id: Math.floor(Math.random() * 903258),
                title,
                text
            }
            const articlesCopy = [...this.state.articles, newArticle]
            this.setState({ articles: articlesCopy}, () => {
                setLocalStorage('myData', articlesCopy);
            });          
        }else {
            alert('Please, fill all input fields')
        }
    }

    render() {

        return(
            <AddPageWrap>
                <h1>Add page</h1>
                <Form onSubmit={this.handleSubmit} articles={this.state.articles} />
                {
                    !this.state.articles ? "no data" : <ArticlesList articles={this.state.articles} amount={3} />
                }
                
            </AddPageWrap>        
        );
    }
}

