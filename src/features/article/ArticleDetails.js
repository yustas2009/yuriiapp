import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { ArticleTitle, ArticleText } from './ArticlePage.styled';

export const ArticleDetails = (props) => {
  const { title, text } = props;
  return(
    <React.Fragment>
      <Row>
        <Col xs="12" md="12">
          <ArticleTitle>
            { title }
          </ArticleTitle>
        </Col>
      </Row>
      <Row>
        <Col xs="12" md="12">
          <ArticleText>
            { text }
          </ArticleText>
        </Col>
      </Row>
    </React.Fragment>
  );
}