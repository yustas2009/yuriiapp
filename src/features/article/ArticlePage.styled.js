import { Grid } from 'react-bootstrap';
import styled from 'styled-components';

export const ArticlePageWrap = styled(Grid)`
  && {
      padding: 50px;
  }
`;

export const ArticleTitle = styled.h1`
  && {
      font-size: 30px;
      text-align: left;
      margin: 30px 0;
  }
`;

export const ArticleText = styled.p`
  && {
      font-size: 16px;
      text-align: left;
  }
`;




