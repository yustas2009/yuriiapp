import React from 'react';
import { ArticlePageWrap } from './ArticlePage.styled';
import { getFromLocalStorage } from '../../util/localStorage';
import { ArticleDetails } from './ArticleDetails';


export class ArticlePageContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            data: []
        };
    }

    componentDidMount() {
        const data = getFromLocalStorage('myData');
        this.setState({ data });
    }

    render() {
        const { data } = this.state;
        return(
            <React.Fragment>
                <ArticlePageWrap>
                  { data.map((article) => {
                      if (this.props.match.params.id && article.id === Number(this.props.match.params.id)) {
                        return <ArticleDetails title={article.title} text={article.text} />;
                      }
                      return false;
                  }) 
                  }
                </ArticlePageWrap>
            </React.Fragment>    
        );
    }
}