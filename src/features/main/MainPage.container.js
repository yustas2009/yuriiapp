import React from 'react';
import { MainPageWrap, ArticleSpan } from './Main.styled';
import { Row, Col } from 'react-bootstrap';
import { MainButton } from '../../components/common/Buttons/MainButton';
import ArticlesList from '../../components/common/Articles/ArticlesList.component';
import { getFromLocalStorage } from '../../util/localStorage';

export class MainPageContainer extends React.Component {
    constructor() {
        super();
        this.state = {
          articles: [],
        }
    }
    
    componentDidMount() {
        try {
            const myData = getFromLocalStorage('myData');
            this.setState({ articles: myData });
        } catch (e) {
            this.setState({ articles: [] });
        }     
    }

    clickHandler = (e) => {
        e.preventDefault();
        this.props.history.push('/add');
    }

    render() {  
        return(
            <React.Fragment>
                <MainPageWrap>
                    <Row className="options-row">
                        <Col xs={12} md={2}>
                            <ArticleSpan>Article</ArticleSpan>
                            <MainButton name="Add New" bsStyle="primary" color="#ec6088" onClick={this.clickHandler} />
                        </Col>
                    </Row>
                    <Row>
                        {
                            this.state.articles && !this.state.articles.length ? <div>Loading...</div> : <ArticlesList articles={this.state.articles} amount={55} />
                        }   
                    </Row>
                </MainPageWrap>
            </React.Fragment>    
        );
    }
}