import { Grid } from 'react-bootstrap';
import styled from 'styled-components';

export const MainPageWrap = styled(Grid)`
  && {
      padding: 50px;
  }
`;

export const ArticleSpan = styled.span`
  && {
      margin-right: 15px;
  }
`;



