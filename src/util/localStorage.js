const localStorage = window.localStorage;

// set and stringify data for localStorage
export const setLocalStorage = (dataName, data) => {
  const stringData = JSON.stringify(data);
  localStorage.setItem(dataName, stringData);
}

// get and parse data from localStorage
export const getFromLocalStorage = dataName => JSON.parse(localStorage.getItem(dataName));